select
    {{ dbt.safe_cast("id", api.Column.translate_type("integer")) }} as id,
    name,
    lower(nametype) as type,
    recclass,
    cast(mass as numeric) as weight,
    lower(fall) as fall,
    parse_datetime('%FT%H:%M:%E3S', year) as landing_date,
    reclat,
    reclong,
    concat(reclat, ',', reclong) as coordinates
from 
    {{ source("staging", "meteorite_landings") }}
