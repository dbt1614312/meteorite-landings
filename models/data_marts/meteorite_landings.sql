{{ config(
    materialized='table',
    partition_by={
      "field": "landing_date",
      "data_type": "datetime",
      "granularity": "day"
    }
)}}

select
    *,
    round(reclat, -1) as rounded_latitude
from 
    {{ ref('stg_meteorite_landings') }}
where 
    reclat is not null